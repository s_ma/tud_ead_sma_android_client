package com.example.ca2androidclientsma;

// Model class with matching instance attributes
public class Book {
    private int bookID;
    private String bookTitle;
    private int bookYear;
    // private int authorID;
    // private Author author;

    // Constructor Overloading
    public Book(int id, String title, int year) {
        super();
        this.bookID = id;
        this.bookTitle = title;
        this.bookYear = year;
    }

    public Book(int id, String title) {
        super();
        this.bookID = id;
        this.bookTitle = title;
    }

    public int getId() {
        return bookID;
    }
    public void setId(int id) {
        this.bookID = id;
    }

    public String getBookTitle() {
        return bookTitle;
    }
    public void setBookTitle(String title) {
        this.bookTitle = title;
    }

    public int getBookYear() {
        return bookYear;
    }
    public void setBookYear(int year) {
        this.bookYear = year;
    }
}
