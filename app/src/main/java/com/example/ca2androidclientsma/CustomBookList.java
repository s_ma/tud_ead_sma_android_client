package com.example.ca2androidclientsma;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

// Create the BaseAdapter for Books ListView
// This class is used for populating data in ListVIew.
public class CustomBookList extends BaseAdapter {
    private Activity context;
    ArrayList<Book> books;

    public CustomBookList(Activity context, ArrayList<Book> books) {
        //   super(context, R.layout.row_item, books);
        this.context = context;
        this.books = books;
    }

    public static class ViewHolder
    {
        TextView textViewId;
        TextView textViewBook;
    }

    @SuppressLint("StringFormatMatches")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get view for row item
        View row = convertView;
        LayoutInflater inflater = context.getLayoutInflater();
        ViewHolder vh;

        if(convertView==null) {
            vh=new ViewHolder();
            row = inflater.inflate(R.layout.row_item, null, true);
            vh.textViewId = (TextView) row.findViewById(R.id.textViewId);
            vh.textViewBook = (TextView) row.findViewById(R.id.textViewBook);
            // Store the holder with the view.
            row.setTag(vh);
        }
        else {
            vh = (ViewHolder) convertView.getTag();
        }

        // Fill the text for each attribute in the associated TextView
        vh.textViewId.setText("BookID: " + books.get(position).getId());
        vh.textViewBook.setText("Title: " + books.get(position).getBookTitle());

        return row;
    }

    public long getItemId(int position) {
        return position;
    }

    public Object getItem(int position) {
        return position;
    }

    public int getCount() {
        if(books.size()<=0)
            return 1;
        return books.size();
    }
}
