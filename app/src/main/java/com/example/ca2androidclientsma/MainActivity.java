package com.example.ca2androidclientsma;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import androidx.appcompat.app.AppCompatActivity;

import static android.content.ContentValues.TAG;

public class MainActivity extends AppCompatActivity {
    // UI
    private Button btnSubmit;
    private ProgressDialog progressDialog;
    private Activity activity;
    private ListView listView;

    // I/O
    private String responseText;
    private StringBuffer response;

    // Web Service URI
    private String SERVICE_URI = "https://webservicesma.azurewebsites.net/api/books/";
    // Construct the URL from the SERVICE_URI later
    private URL url;
    // Store the data fetched from the Web Service
    private ArrayList<Book> books = new ArrayList<Book>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // UI
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        listView = (ListView) findViewById(android.R.id.list);

        // Proceeds when the button is clicked
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                books.clear();                      // Empty the ArrayList
                new GetServiceData().execute();     // Call the WebService
            }
        });
    }

    class GetServiceData extends AsyncTask
    {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage(getResources().getString(R.string.loading_data));
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            return getWebServiceResponseData();
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            // Dismiss the progress dialog
            if (progressDialog.isShowing())
                progressDialog.dismiss();

            // Populate list data
            CustomBookList customBookList = new CustomBookList(activity, books);
            listView.setAdapter(customBookList);

            // If selected row
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    Toast.makeText(getApplicationContext(),"Book selected: "+ books.get(position).getBookTitle(),Toast.LENGTH_SHORT).show();
                    // Toast.makeText(getApplicationContext(),getResources().getString(R.string.selected_book) + books.get(position).getBookTitle(),Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    protected Void getWebServiceResponseData() {
        // Setting the connection with the Web Service
        try {
            url = new URL(SERVICE_URI);
            Log.d(TAG, "ServerData: " + SERVICE_URI);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");

            int responseCode = conn.getResponseCode();

            Log.d(TAG, "Response code: " + responseCode);
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                // Reading response from input Stream
                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String output;
                response = new StringBuffer();

                while ((output = in.readLine()) != null) {
                    response.append(output);
                }
                in.close();
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        responseText = response.toString();
        Log.d(TAG, "data:" + responseText);

        // Populate the ArrayList for display
        try {
            JSONArray jsonarray = new JSONArray(responseText);
            for (int i = 0; i < jsonarray.length(); i++) {
                // Get the current JSON instance
                JSONObject jsonobject = jsonarray.getJSONObject(i);
                // Retrieve its attributes
                int id = jsonobject.getInt("bookID");
                String bookTitle = jsonobject.getString("bookTitle");
                Log.d(TAG, "BookID:" + id);
                Log.d(TAG, "BookTitle:" + bookTitle);
                // Create a new Book instance from these attributes and append it to the ArrayList
                Book BookObj = new Book(id, bookTitle);
                books.add(BookObj);
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

}